﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class FishManager : MonoBehaviour
{
    SpriteRenderer sr;

    public float left, right;
    Vector3 move = Vector3.left;

    // Start is called before the first frame update
    void Start()
    {
        sr = GetComponent<SpriteRenderer>();
    }

    // Update is called once per frame
    void Update()
    {
        MoveLeftAndRight();
    }

    private void OnCollisionEnter2D(Collision2D collision)
    {
        if (collision.collider.gameObject.CompareTag("Ball"))
        {
            GameObject ball = collision.collider.gameObject;
            
            sr.color = ball.GetComponent<SpriteRenderer>().color;

            AudioManager.Instance.Play("FishEat", 3.0f);

            Destroy(ball);
        }
    }

    void MoveLeftAndRight()
    {
        if (transform.position.x < left)
        {
            move = Vector3.right;
            sr.flipX = true;
        }
        else if(transform.position.x > right)
        {
            move = Vector3.left;
            sr.flipX = false;
        }
        
        transform.position += move * Time.deltaTime;
    }
}
