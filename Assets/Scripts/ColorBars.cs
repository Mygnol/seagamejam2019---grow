﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class ColorBars : MonoBehaviour
{
    public GameObject tankObject;
    TankBottom tankScript;

    public RectTransform barR;
    public RectTransform barG;
    public RectTransform barB;

    public RectTransform targetR;
    public RectTransform targetG;
    public RectTransform targetB;

    public Button buttonR;
    public Button buttonG;
    public Button buttonB;

    public RectTransform highlightR;
    public RectTransform highlightG;
    public RectTransform highlightB;

    public float responseSpeed = 1f;

    float defaultBarSize;
    float defaultBarPosition;
    float defaultTargetPosition;

    float currentR, currentG, currentB;

    static bool playBarIncreaseSound;

    // Start is called before the first frame update
    void Start()
    {
        tankScript = tankObject.GetComponent<TankBottom>();
        defaultBarSize = barR.sizeDelta.y;
        defaultBarPosition = barR.position.y;

        defaultTargetPosition = targetR.position.y;

        barR.sizeDelta = new Vector2(20, 0);
        barG.sizeDelta = new Vector2(20, 0);
        barB.sizeDelta = new Vector2(20, 0);

        if (tankScript == null)
        {
            tankScript = FindObjectOfType<TankBottom>();
        }
    }

    // Update is called once per frame
    void Update()
    {
        //Modify Color Indicators
        if (currentR != tankScript.StoredColor.r)
        {
            if (currentR > tankScript.StoredColor.r + 0.01)
                currentR -= Time.deltaTime;
            else if (currentR < tankScript.StoredColor.r - 0.01)
                currentR += Time.deltaTime;
            else
            {
                playBarIncreaseSound = true;

                currentR = tankScript.StoredColor.r;
            }

            barR.sizeDelta = new Vector2(30, currentR * defaultBarSize);
        }
        if (currentG != tankScript.StoredColor.g)
        {
            if (currentG > tankScript.StoredColor.g + 0.01)
                currentG -= Time.deltaTime;
            else if (currentG < tankScript.StoredColor.g - 0.01)
                currentG += Time.deltaTime;
            else
            {
                playBarIncreaseSound = true;

                currentG = tankScript.StoredColor.g;
            }

            barG.sizeDelta = new Vector2(30, currentG * defaultBarSize);
            
        }

        if (currentB != tankScript.StoredColor.b)
        {
            if (currentB > tankScript.StoredColor.b + 0.01)
                currentB -= Time.deltaTime;
            else if (currentB < tankScript.StoredColor.b - 0.01)
                currentB += Time.deltaTime;
            else
            {
                playBarIncreaseSound = true;

                currentB = tankScript.StoredColor.b;
            }

            barB.sizeDelta = new Vector2(30, currentB * defaultBarSize);

        }

        //Modify Target Indicators
        targetR.position = new Vector2(targetR.position.x, defaultTargetPosition + (tankScript.redMin * defaultBarSize * 2.5f));
        targetR.sizeDelta = new Vector2(targetR.sizeDelta.x, (tankScript.redMax * defaultBarSize) - (tankScript.redMin * defaultBarSize));
        
        targetG.position = new Vector2(targetG.position.x, defaultTargetPosition + (tankScript.greenMin * defaultBarSize * 2.5f));
        targetG.sizeDelta = new Vector2(targetG.sizeDelta.x, (tankScript.greenMax * defaultBarSize) - (tankScript.greenMin * defaultBarSize));

        targetB.position = new Vector2(targetB.position.x, defaultTargetPosition + (tankScript.blueMin * defaultBarSize * 2.5f));
        targetB.sizeDelta = new Vector2(targetB.sizeDelta.x, (tankScript.blueMax * defaultBarSize) - (tankScript.blueMin * defaultBarSize));

        //Disable buttons when in range
        if (tankScript.redWithinRange)
        {
            buttonR.interactable = false;
            highlightR.GetComponent<Image>().color = Color.yellow;
        }
        else
        {
            buttonR.interactable = true;
            highlightR.GetComponent<Image>().color = Color.red;
        }

        if (tankScript.greenWithinRange)
        {
            buttonG.interactable = false;
            highlightG.GetComponent<Image>().color = Color.yellow;
        }
        else
        {
            buttonG.interactable = true;
            highlightG.GetComponent<Image>().color = Color.green;
        }

        if (tankScript.blueWithinRange)
        {
            buttonB.interactable = false;
            highlightB.GetComponent<Image>().color = Color.yellow;
        }
        else
        {
            buttonB.interactable = true;
            highlightB.GetComponent<Image>().color = Color.blue;
        }

        if(playBarIncreaseSound)
        {
            AudioManager.Instance.Play("BarIncrease");

            playBarIncreaseSound = false;
        }
    }
}
