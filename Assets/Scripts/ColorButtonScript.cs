﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;

public enum BUTTON_COLOR
{
    RED = 0,
    GREEN,
    BLUE
}

public class ColorButtonScript : MonoBehaviour
{
    public TankBottom colorScript;
    public BUTTON_COLOR color = BUTTON_COLOR.BLUE;

    private void Start()
    {
        if(colorScript == null)
        {
            colorScript = FindObjectOfType<TankBottom>();
        }
    }

    public void OnClick()
    {
        if (color == BUTTON_COLOR.RED)
            colorScript.StoredColor.r = colorScript.StoredColor.r * 0.5f;

        else if (color == BUTTON_COLOR.GREEN)
            colorScript.StoredColor.g = colorScript.StoredColor.g * 0.5f;

        else if (color == BUTTON_COLOR.BLUE)
            colorScript.StoredColor.b = colorScript.StoredColor.b * 0.5f;

        AudioManager.Instance.Play("Button");
    }
}
