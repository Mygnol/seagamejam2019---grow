﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;
using System.Collections.Generic;
using System;
using System.IO;

public class Snapshot : MonoBehaviour
{
    public RenderTexture _renderTexture;
    public RawImage capturedImage;
    public PelletDispenser dispenser;
    Camera _cam;
    Texture2D _texture2d;

    public RawImage display;
    public Image mask;
    public Button swapButton;
    public Button snapButton;

    public GameObject photoFrame;

    // Start is called before the first frame update
    void Start()
    {
        _cam = FindObjectOfType<Camera>();
        if (_cam == null)
        {
            Debug.LogError("No Camera found!");
            //errorCam.text = "No Camera found!";
            Destroy(this);
        }
        capturedImage.gameObject.SetActive(false);
    }

    // Update is called once per frame
    public void SnapPhoto()
    {
        dispenser.CreatePelletFromTexture(null);
        //StartCoroutine(CaptureScreenshot(_cam, _renderTexture));

        display.gameObject.SetActive(false);
        mask.gameObject.SetActive(false);
        swapButton.gameObject.SetActive(false);
        snapButton.gameObject.SetActive(false);
    }

    IEnumerator CaptureScreenshot(Camera _cam, RenderTexture rTex /*string _path*/)
    {
        yield return new WaitForSecondsRealtime(1f);
        //yield return new WaitForEndOfFrame();

        

        if (rTex.width != Screen.width || rTex.height != Screen.height)
        {
            //CreateTextures();
            rTex = new RenderTexture(Screen.width, Screen.height, 24);
            _texture2d = new Texture2D(Screen.width, Screen.height);

            capturedImage.texture = _texture2d;
            capturedImage.gameObject.SetActive(true);

            if(_texture2d != null)
            {
                dispenser.CreatePelletFromTexture(_texture2d);
            }
        }

        //Graphics.Blit(_cam.targetTexture, rTex);
        RenderTexture.active = rTex;

        _texture2d.ReadPixels(new Rect(0, 0, _texture2d.width, _texture2d.height), 0, 0);
        _texture2d.Apply();

        //byte[] bytes;
        //bytes = _texture2d.EncodeToPNG();
        //File.WriteAllBytes(_path, bytes);
        //_texture2d.LoadImage(bytes);

        
    }
}
