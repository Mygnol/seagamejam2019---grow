﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class RenderCamera : MonoBehaviour
{
    public RawImage display;
    public Image mask;
    public Button swapButton;
    public Button snapButton;

    WebCamTexture wbTex;

    int currentCamIndex = 0;

    // Start is called before the first frame update
    void Start()
    {
        display.gameObject.SetActive(false);
        mask.gameObject.SetActive(false);
        swapButton.gameObject.SetActive(false);
        snapButton.gameObject.SetActive(false);
    }

    public void SwapCam_Clicked()
    {
        if(WebCamTexture.devices.Length>0)
        {
            currentCamIndex += 0;
            currentCamIndex %= WebCamTexture.devices.Length;
            Debug.Log("Testing Camera" + currentCamIndex);
        }
        else
        {
            Debug.Log("No cameras!");
        }

        ToggleCam_Clicked();
    }

    public void ToggleCam_Clicked()
    {
        // Stop the Camera
        if(wbTex != null)
        {
            display.texture = null;
            wbTex.Stop();
            wbTex = null;
            display.gameObject.SetActive(false);
            mask.gameObject.SetActive(false);
            swapButton.gameObject.SetActive(false);
            snapButton.gameObject.SetActive(false);
        }
        else
        // Start the Camera
        {
            if (WebCamTexture.devices.Length > 0)
            {
                WebCamDevice device = WebCamTexture.devices[currentCamIndex];
                wbTex = new WebCamTexture(device.name);

                display.texture = wbTex;

                float antiRotate = -(90 - wbTex.videoRotationAngle);
                Quaternion quatROT = new Quaternion();
                quatROT.eulerAngles = new Vector3(0, 0, antiRotate);
                display.transform.rotation = quatROT;

                wbTex.Play();
            }
            display.gameObject.SetActive(true);
            mask.gameObject.SetActive(true);
            swapButton.gameObject.SetActive(true);
            snapButton.gameObject.SetActive(true);
        }
    }

    // Update is called once per frame
    void Update()
    {
        
    }
}
