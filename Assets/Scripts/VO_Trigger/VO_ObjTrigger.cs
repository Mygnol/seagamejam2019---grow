﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class VO_ObjTrigger : MonoBehaviour
{
    public List<GameObject> mAudioVO;
    
    public void EnableTargetAudio(int mlist)
    {
        foreach(GameObject i in mAudioVO)
        {
            i.SetActive(false);
        }
        mAudioVO[mlist].SetActive(true);
    }

    // Start is called before the first frame update
    void Start()
    {
        foreach (GameObject i in mAudioVO)
        {
            i.SetActive(false);
        }
    }

    // Update is called once per frame
    void Update()
    {
        
    }
}
