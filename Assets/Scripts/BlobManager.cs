﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

enum GrowthStage
{
    STAGE_1,
    STAGE_2,
    STAGE_3,
    STAGE_4
}

public class BlobManager : MonoBehaviour
{
    public Sprite growth1;
    public Sprite growth2;
    public Sprite growth3;
    public Sprite growth4;

    GrowthStage myGrowth;

    SpriteRenderer sr;

    float angle;

    // Start is called before the first frame update
    void Start()
    {
        myGrowth = GrowthStage.STAGE_2;
        sr = GetComponent<SpriteRenderer>();
        sr.sprite = growth1;
        
    }

    // Update is called once per frame
    void Update()
    {
        FirstStage();
        SecondStage();
    }

    void FirstStage()
    {
        if(myGrowth == GrowthStage.STAGE_1)
        {
            if(sr.sprite != growth1)
            {
                sr.sprite = growth1;
            }

            if (transform.localScale.x <= 1)
            {
                transform.localScale += Vector3.right * Time.deltaTime;
            }
            else if(transform.localScale.x >= 1.5f)
            {
                transform.localScale -= Vector3.right * Time.deltaTime;
            }

            if (transform.localScale.y <= 1)
            {
                transform.localScale += Vector3.up * Time.deltaTime;
            }
            else if (transform.localScale.y >= 1.5f)
            {
                transform.localScale -= Vector3.up * Time.deltaTime;
            }
        }
    }

    void SecondStage()
    {
        if (myGrowth == GrowthStage.STAGE_2)
        {
            if (sr.sprite != growth2)
            {
                sr.sprite = growth2;
            }
            
            angle += 0.05f;

            float d = Mathf.Sin(angle + 90) * 0.15f;
            
            transform.Rotate(0, 0, d);
        }
    }
}
