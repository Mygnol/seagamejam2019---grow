﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TankBottom : MonoBehaviour
{
    [Range(0.0F, 1.0F)]
    public float redMax, redMin, greenMax, greenMin, blueMax, blueMin;
    public bool redWithinRange, blueWithinRange, greenWithinRange;

    public int currentLevel = 0;

    BoxCollider2D bc;

    public ParticleSystem PS_BallDestoryed;
    public ParticleSystem PS_Poof;

    public Color StoredColor;

    // Start is called before the first frame update
    void Start()
    {

        StoredColor.a = 255;
        bc = GetComponent<BoxCollider2D>();
        SetTargetBars(0.2f);

        AllBlobManager.Instance.SwitchBlob(1);
    }

    // Update is called once per frame
    void Update()
    {
        if (StoredColor.r >= redMin && StoredColor.r <= redMax)
            redWithinRange = true;
        else
            redWithinRange = false;

        if (StoredColor.g >= greenMin && StoredColor.g <= greenMax)
            greenWithinRange = true;
        else
            greenWithinRange = false;

        if (StoredColor.b >= blueMin && StoredColor.b <= blueMax)
            blueWithinRange = true;
        else
            blueWithinRange = false;

        //Check for level progression
        if(redWithinRange && greenWithinRange && blueWithinRange)
        {
            
            if(currentLevel <= 2)
            {
                PS_Poof.Play();
                currentLevel++;
                SetTargetBars(0.2f - 0.01f * currentLevel);
                StoredColor = new Color32(0, 0, 0, 255);

                AudioManager.Instance.Play("StageClear1");
                AllBlobManager.Instance.SwitchBlob(2);
            }
            else if (currentLevel <= 4)
            {
                PS_Poof.Play();
                currentLevel++;
                SetTargetBars(0.2f - 0.01f * currentLevel);
                StoredColor = new Color32(0, 0, 0, 255);

                AudioManager.Instance.Play("StageClear2");
                AllBlobManager.Instance.SwitchBlob(3);
            }
            else if (currentLevel <= 6)
            {
                PS_Poof.Play();
                currentLevel++;
                SetTargetBars(0.2f - 0.01f * currentLevel);
                StoredColor = new Color32(0, 0, 0, 255);

                AudioManager.Instance.Play("StageClear3");

                AudioManager.Instance.Stop("BGM");
                AudioManager.Instance.Play("BGM2");

                AllBlobManager.Instance.SwitchBlob(4);
            }

            Debug.Log("Level" + currentLevel);
        }

    }

    void SetTargetBars(float minimumSpacing)
    {
        redMax = Random.Range(40,100)*0.01f;
        redMin = Random.Range(0.2f, redMax - minimumSpacing);

        greenMax = Random.Range(40, 100) * 0.01f;
        greenMin = Random.Range(0.2f, greenMax - minimumSpacing);

        blueMax = Random.Range(40, 100) * 0.01f;
        blueMin = Random.Range(0.2f, blueMax - minimumSpacing);
    }

    private void OnCollisionEnter2D(Collision2D collision)
    {
        if (collision.collider.gameObject.CompareTag("Ball"))
        {
            GameObject ball = collision.collider.gameObject;
            
            if (currentLevel < 6)
            {
                if (!redWithinRange && ball.GetComponent<SpriteRenderer>().color.r != 0)
                {
                    if (StoredColor.r != 0)
                        StoredColor.r = (StoredColor.r * 0.6f + ball.GetComponent<SpriteRenderer>().color.r * 0.6f);
                    else
                        StoredColor.r = ball.GetComponent<SpriteRenderer>().color.r;
                }

                if (!greenWithinRange && ball.GetComponent<SpriteRenderer>().color.g != 0)
                {
                    if (StoredColor.g != 0)
                        StoredColor.g = (StoredColor.g * 0.6f + ball.GetComponent<SpriteRenderer>().color.g * 0.6f);
                    else
                        StoredColor.g = ball.GetComponent<SpriteRenderer>().color.g;
                }

                if (!blueWithinRange && ball.GetComponent<SpriteRenderer>().color.b != 0)
                {
                    if (StoredColor.b != 0)
                        StoredColor.b = (StoredColor.r * 0.6f + ball.GetComponent<SpriteRenderer>().color.b * 0.6f);
                    else
                        StoredColor.b = ball.GetComponent<SpriteRenderer>().color.b;
                }
            }

            PS_BallDestoryed.transform.position = collision.GetContact(0).point;

            PS_BallDestoryed.Play();

            Destroy(ball);
        }
    }
    
}
