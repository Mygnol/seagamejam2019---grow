﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BallManager : MonoBehaviour
{
    private Vector3 screenPoint;
    private Vector3 offset;
    public bool IsDragable = true;

    bool inWater = false;
    public ParticleSystem PS_Water;
    ParticleSystem temp;
    
    bool isDragging = false;
    bool playPickUpSound = false;

    Rigidbody2D rb;

    BoxCollider2D touchCollider;

    // Start is called before the first frame update
    void Start()
    {
        rb = GetComponent<Rigidbody2D>();
        Physics.queriesHitTriggers = true;

        touchCollider = GetComponent<BoxCollider2D>();
    }

    // Update is called once per frame
    void Update()
    {
        if(inWater)
        {
            FallSlowly();
        }

        if (temp != null)
        {
            if (!temp.IsAlive())
            {
                Destroy(temp.gameObject);
            }
        }

        if(playPickUpSound)
        {
            float r = Random.Range(2.4f, 3.0f);
            // just for us to see
            AudioManager.Instance.sounds[4].pitch = r;

            AudioManager.Instance.Play("PickUp", r);

            playPickUpSound = false;
        }
    }

    void OnMouseDown()
    {
        Vector3 scanPos = transform.position;
        screenPoint = Camera.main.WorldToScreenPoint(scanPos);

        offset = scanPos - Camera.main.ScreenToWorldPoint(
           new Vector3(Input.mousePosition.x, Input.mousePosition.y, screenPoint.z));

        isDragging = true;

        if (!playPickUpSound)
        {
            playPickUpSound = true;
        }
    }
    
    void OnMouseDrag()
    {
        Vector3 curScreenPoint = new Vector3(Input.mousePosition.x, Input.mousePosition.y, screenPoint.z);
        
        Vector3 curPosition = Camera.main.ScreenToWorldPoint(curScreenPoint) + offset;
        //transform.position = curPosition;

        if(isDragging)
        {

            rb.AddForce((curPosition - transform.position) * 100 - new Vector3(rb.velocity.x, rb.velocity.y, 0) * 20);
        }
        
    }

    void FallSlowly()
    {
        Vector2 max = Vector2.ClampMagnitude(rb.velocity, 2);
        rb.velocity = max;
    }

    private void OnCollisionEnter2D(Collision2D collision)
    {
        if(collision.gameObject.CompareTag("Wall"))
        {
            isDragging = false;

            AudioManager.Instance.Play("BallHit");
        }
    }
    
    private void OnTriggerEnter2D(Collider2D other)
    {
        if (other.gameObject.CompareTag("Water"))
        {
            Physics2D.IgnoreCollision(other, touchCollider);

            Vector2 v = new Vector2(transform.position.x, transform.position.y - 2.0f);
            if(temp == null)
            {
                AudioManager.Instance.Play("BallHitWater");

                temp = Instantiate(PS_Water, v, Quaternion.identity);

                temp.trigger.SetCollider(0, other);

                temp.Play();
            }
            
            inWater = true;
        }
    }

    private void OnTriggerExit2D(Collider2D other)
    {
        if (other.gameObject.CompareTag("Water"))
        {
            inWater = false;
        }
    }
}
