﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PelletDispenser : MonoBehaviour
{
    public Texture2D dispensedTexture;
    public GameObject pelletObject;
    public bool canDispense = false;

    // Update is called once per frame
    void Update()
    {
        if(canDispense)
        {
            canDispense = false;
            //CreatePelletFromTexture(dispensedTexture);
        }
    }

    public void ToggleDispense()
    {
        canDispense = true;
    }

    public void CreatePelletFromTexture(Texture2D tex,int sampleEveryXPixels = 1)
    {
        Debug.Log("TEST");
        //Create the pellet object
        GameObject newObj = Object.Instantiate(pelletObject, this.transform.position, Quaternion.identity);

        if (tex != null)
        {
            Color32[] texColors = tex.GetPixels32();
            int total = texColors.Length;

            float r = 0;
            float g = 0;
            float b = 0;

            for (int i = 0; i < total; i += sampleEveryXPixels)
            {
                r += texColors[i].r;
                g += texColors[i].g;
                b += texColors[i].b;
            }

            newObj.GetComponent<SpriteRenderer>().color = new Color32((byte)(r/total), (byte)(g/total), (byte)(b/total), 255);
        }
        else
        {
            Debug.Log("Invalid Texture!");
            newObj.GetComponent<SpriteRenderer>().color = new Color32((byte)Random.Range(0,255), (byte)Random.Range(0, 255), (byte)Random.Range(0, 255), 255);
        }
    }
}
