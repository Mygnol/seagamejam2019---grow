﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AllBlobManager : MonoBehaviour
{
    public GameObject Blob1;
    public GameObject Blob2;
    public GameObject Blob3;
    public GameObject Blob4;

    public Transform tank;

    public Vector2[] pos;

    public int n;

    public Canvas c;

    static AllBlobManager instance;
    public static AllBlobManager Instance { get { return instance; } }

    private void Awake()
    {
        if(instance == null)
        {
            instance = this;
        }
        else
        {
            Destroy(this.gameObject);
        }
    }

    // Start is called before the first frame update
    void Start()
    {
        n = 1;
        AllBlobManager.Instance.SwitchBlob(1);
    }

    // Update is called once per frame
    void Update()
    {
        if (n == 1)
        {
            tank.position = pos[0];

            Blob1.SetActive(true);
            Blob2.SetActive(false);
            Blob3.SetActive(false);
            Blob4.SetActive(false);
        }
        else if (n == 2)
        {
            tank.position = pos[1];

            Blob1.SetActive(false);
            Blob2.SetActive(true);
            Blob3.SetActive(false);
            Blob4.SetActive(false);
        }
        else if (n == 3)
        {
            tank.position = pos[2];

            Blob1.SetActive(false);
            Blob2.SetActive(false);
            Blob3.SetActive(true);
            Blob4.SetActive(false);
        }
        else if (n == 4)
        {
            tank.position = pos[3];

            Blob1.SetActive(false);
            Blob2.SetActive(false);
            Blob3.SetActive(false);
            Blob4.SetActive(true);

            c.enabled = false;
        }
    }

    public void SwitchBlob(int v)
    {
        this.n = v;
        if(n == 1)
        {
            tank.position = pos[0];

            Blob1.SetActive(true);
            Blob2.SetActive(false);
            Blob3.SetActive(false);
            Blob4.SetActive(false);
        }
        else if (n == 2)
        {
            tank.position = pos[1];

            Blob1.SetActive(false);
            Blob2.SetActive(true);
            Blob3.SetActive(false);
            Blob4.SetActive(false);
        }
        else if (n == 3)
        {
            tank.position = pos[2];

            Blob1.SetActive(false);
            Blob2.SetActive(false);
            Blob3.SetActive(true);
            Blob4.SetActive(false);
        }
        else if (n == 4)
        {
            tank.position = pos[3];

            Blob1.SetActive(false);
            Blob2.SetActive(false);
            Blob3.SetActive(false);
            Blob4.SetActive(true);

            c.enabled = false;
        }
    }
}
