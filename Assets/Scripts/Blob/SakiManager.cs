﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SakiManager : MonoBehaviour
{
    Animator anim;

    /// <summary>
    /// 0 = idle, 1 = talking1, 2 = sleepy, 3 = talking2, 4 = surprise(click)
    /// </summary>

    public float stimer = 0;
    public float ctimer = 0;
    float duration = 3;

    public bool canChangeState;
    public bool isSurprised;

    // Start is called before the first frame update
    void Start()
    {
        anim = GetComponent<Animator>();
    }

    // Update is called once per frame
    void Update()
    {
        if(!isSurprised)
        {
            if (canChangeState)
            {
                int r = Random.Range(1, 4);

                anim.SetInteger("State", r);

                canChangeState = false;
            }
            else
            {
                if (ctimer >= duration)
                {
                    anim.SetInteger("State", 0);

                    if (anim.GetInteger("State") == 0 && anim.GetCurrentAnimatorStateInfo(0).IsName("Idle"))
                    {
                        canChangeState = true;
                        ctimer = 0;
                    }
                }
                else
                {
                    ctimer += Time.deltaTime;
                }
            }
        }
        else if(isSurprised)
        {
            stimer += Time.deltaTime;
            if(stimer >= duration)
            {
                anim.SetInteger("State", 0);
                isSurprised = false;
                stimer = 0;
            }
        }
      /*  else
        {
            if (isSurprised && stimer >= duration)
            {
                anim.SetInteger("State", 0);

                if (anim.GetInteger("State") == 0 && anim.GetCurrentAnimatorStateInfo(0).IsName("Idle"))
                {
                    isSurprised = false;
                    stimer = 0;

                }
            }
            else
            {
                stimer += Time.deltaTime;
            }
        }*/

       
    }
    
    private void OnMouseDown()
    {
        anim.SetInteger("State", 4);

        stimer = 0;
        ctimer = duration;

        isSurprised = true;

        canChangeState = false;
    }
}
