﻿using UnityEngine.Audio;
using UnityEngine;
using System;

public class AudioManager : MonoBehaviour {
    
    static AudioManager instance;
    public static AudioManager Instance { get { return instance; } }

    public Sound[] sounds;
    
	// Use this for initialization
	void Awake () {

        if (instance == null)
        {
            instance = this;
        }else
        {
            Destroy(this.gameObject);
        }


		foreach(Sound s in sounds)
        {
            s.source = gameObject.AddComponent<AudioSource>();
            s.source.clip = s.clip;
            s.source.volume = s.volume;
            s.source.pitch = s.pitch;
            s.source.loop = s.loop;
        }

	}

    void Start()
    {
        Play("BGM");

        DontDestroyOnLoad(this.gameObject);

   //     Play("Explosion1");
   //     Play("Environmental");
   //     Play("InGameBGM");
    }
    

    public void Play (string name)
    {
        Sound s = Array.Find(sounds, sound => sound.name == name);
        s.source.Play();
    }

    public void Play(string name, float pitch)
    {
        Sound s = Array.Find(sounds, sound => sound.name == name);

        s.source.pitch = pitch;

        s.source.Play();
    }

    public void Stop(string name)
    {
        Sound s = Array.Find(sounds, sound => sound.name == name);
        s.source.Stop();
    }
    
}
