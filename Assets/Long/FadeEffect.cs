﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class FadeEffect : MonoBehaviour
{
    Image rend;

    public Color startColor;
    public Color endColor;

    public float moveTime;
    float currTime = 0;

    private void Start()
    {
        rend = GetComponent<Image>();
        rend.enabled = true;
    }

    // Update is called once per frame
    void Update()
    {
        currTime += Time.deltaTime;
        rend.color = Color.Lerp(endColor, startColor, currTime / moveTime);
    }
}
