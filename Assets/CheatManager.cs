﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CheatManager : MonoBehaviour
{
    public TankBottom tb;

    // Start is called before the first frame update
    void Start()
    {

        Physics2D.queriesHitTriggers = true;
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    private void OnMouseDown()
    {
        tb.redWithinRange = true;
        tb.blueWithinRange = true;
        tb.greenWithinRange = true;
    }
   
}
