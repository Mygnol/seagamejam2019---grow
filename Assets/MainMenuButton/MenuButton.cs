﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class MenuButton : MonoBehaviour
{
    public Sprite playDark;

    public void ChangeScene(string s)
    {
        SceneManager.LoadScene(s);
    }

    public void PlayButtonClicked()
    {
        GetComponent<Image>().sprite = playDark;
    }
}
